#import "../theme.typ": *
#show: theme

#section[Traits]

#slide(
  title: [Traits],
)[
Java: `Interface`\
Ärver från 0|1 klass, implementerar 0+ interfaces

Python: `typing.Protocol` (?)

#pause
I både Java och Rust säger vi att vi "implementerar" ett Interface / en Trait

#pause

```java
public class Member implements Comparable<Member> {
  //                           ^^^^^^^^^^^^^^^^^^
  //                                            |
  //   en Member kan jämföras med en annan Member
  private String name;
  private int height;
  // ...

  // Comparable<Member> säger att den här metoden
  // måste implementeras
  @Override
  public int compareTo(Member member) {
    return height.compareTo(member.getHeight());
  }
}
```
]

#slide(title: [`Comparable` i Rust])[
Rust använder `PartialOrd` (partialordning) och `Ord` (totalordning)

```rust
struct Member {
  name: String,
  height: i32,
}

impl PartialOrd for Member {
  fn partial_cmp(&self, other: &Member) -> Option<Ordering> {
    self.name.partial_cmp(&other.name)
  }
}
```

#pause
Varför `Option<Ordering>` i `PartialOrd`?
]

#slide(title: [Traits kan bero på andra trait])[
```
error[E0277]: can't compare `Member` with `Member`
    --> src/main.rs:8:21
     |
8    | impl PartialOrd for Member {
     |                     ^^^^^^ no implementation
                                  for `Member == Member`
     |
     = help: the trait `PartialEq` is not implemented for `Member`
note: required by a bound in `PartialOrd`
    --> .../core/src/cmp.rs:1085:43
     |
1085 | pub trait PartialOrd<Rhs: ?Sized = Self>: PartialEq<Rhs> {
     |                                           ^^^^^^^^^^^^^^
         required by this bound in `PartialOrd` /

```

#pause
#skip.large

```rust
trait ThisTrait: OtherTrait {}
// Du behöver implementera OtherTrait
// för att kunna implementera ThisTrait
```
]

#slide(title: [])[
```rust
impl PartialOrd for Member {
  fn partial_cmp(&self, other: &Member) -> Option<Ordering> {
    self.name.partial_cmp(&other.name)
  }
}

impl PartialEq for Member {
  fn eq(&self, other: &Member) -> bool {
    self.name.eq(&other.name)
  }
}
```

#pause
Varför inte `Option<bool>` i `PartialEq`?
]

#slide(
  title: [Lexikografisk sortering],
)[
Ofta vill vi att en struct ska sorteras lexikografiskt: Börja med första fältet,
om dom är lika jämför istället andra fältet, osv.

#pause

Enkelt för `PartialEq`:

```rust
impl PartialEq for Member {
  fn eq(&self, other: &Member) -> bool {
    self.name.eq(&other.name) || self.height.eq(&other.height)
  }
}
```

#pause

Krångligare för `PartialOrd`:

```rust
impl PartialOrd for Member {
    fn partial_cmp(&self, other: &Member) -> Option<Ordering> {
        Some(
            self.name
                .partial_cmp(&other.name)?
                .then(self.height.partial_cmp(&other.height)?),
        )
    }
}

```
]

#slide(
  title: [Makron som implementerar traits],
)[
Vi kan automatiskt generera en lexikografisk jämförelse för `PartialEq` och
`PartialOrd`: ```rust
#[derive(PartialEq, PartialOrd)]
struct Member {
  name: String,
  height: i32,
}
```

#pause

Fler inbyggda `#[derive]`-makron:

#line-by-line[

- Eq, Ord (vanligt att plocka alla fyra)
- Clone -- `member.clone()`
- Copy -- stödjer "bit-copy" / `memcpy`
- Default -- `Member::default()` initialiserar med `T::default()` på alla fält.
  - `String::default() == ""`
  - `i32::default() == 0`
- Hash -- behövs för att använda som nyckel i en HashMap
- Debug
]
]
