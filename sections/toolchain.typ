#import "../theme.typ": *
#show: theme

#section[Toolchain]

#slide(title: [Vad är en toolchain?])[
  Alla program som behövs för att kompilera vår kod:

  - Byggsystem (cargo)
  - Kompilator (rustc)
  - Standardbibliotek
  - Länkare
  - Andra verktyg (clippy, rustfmt, rust-analyzer)

  Vissa saker förväntas finnas installerade:
  - Länkare (ofta GCC/Clang, finns andra)
  - `libc`, andra bibliotek


  #pause
  En toolchain skrivs `<channel>[-<date>][-<host>]`

]

#slide(title: [Channel])[
  `<channel>[-<date>][-<host>]`

   Källkod på GitHub: `rust-lang/rust`, `rust-lang/cargo`, ...

#line-by-line(start: 2)[
   - `nightly`: ny version varje dag (t.ex. `nightly-2022-11-27`)
   - `beta`: branchas från `nightly` var 6:e vecka
   - `stable`: uppgraderad från `beta` efter 6 veckor av test/buggfix
]

  #uncover("4-")[
    Dessutom `1.50`, `1.71.1` som pekar på motsvarande `stable`-version
  ]

  #uncover("5-")[
   Vissa delar av kompilatorn/standardbiblioteket finns bara på `nightly`

   #image(height: 5em, "../nightly-only.png")
  ]
]

#slide(title: [Host])[
  `<channel>[-<date>][-<host>]`

  Host (eller "target") namnges av en "target triple":

  #line-by-line[
  - `x86_64-unknown-linux-gnu` (länka med glibc, gcc-libs runtime)
  - `x86_64-unknown-linux-musl` (länka med musl, statisk runtime)
  - `x86_64-pc-windows-msvc` (microsofts toolchain)
  - `x86_64-pc-windows-gnu` (mingw, cross-compilation)
  - `x86_64-apple-darwin` (x86-64 macOS)
  - `aarch64-apple-darwin` (apple silicon macOS)
  - `wasm32-unknown-emscripten`
  - `riscv32i-unknown-none-elf`
  ]
]

#slide(title: [Cargo -- byggsystem])[
  - Laddar ner dependencies som behövs
  - Kompilerar med rätt kompilator
  - Hanterar olika targets

#pause
```
$ cargo build --target x86_64-pc-windows-gnu
  Compiling hello-world v0.1.0 (.../hello-world)
   Finished dev [unoptimized + debuginfo] target(s) in 0.31s
$ ls target/x86_64-pc-windows-gnu/debug/
build  deps  examples  hello-world.d  hello-world.exe  incremental
                                      ^^^^^^^^^^^^^^^

```

#pause
Installerar _inte_ en hel toolchain:

```
$ cargo build --target x86_64-pc-windows-gnu
  Compiling hello-world v0.1.0 (.../hello-world)
error: linker `x86_64-w64-mingw32-gcc` not found
  |
  = note: No such file or directory (os error 2)
```
]

#slide[
```
$ cargo run --target x86_64-pc-windows-gnu
   Running `target/x86_64-pc-windows-gnu/debug/hello-world.exe`
007c:fixme:wineusb:query_id Unhandled ID query type 0x5.
Hello, world!
```
]

#slide(title: [Rustc -- kompilatorn])[
  - Kompilerar till objektfiler och körbara filer, precis som GCC/Clang
  - Kan användas direkt i terminalen (ovanligt) eller med andra byggsystem (inte lika ovanligt, men ganska ovanligt)

#pause
```
$ cat hello-world.rs
fn main() {
  println!("hello world!");
}
$ rustc hello-world.rs
$ ./hello-world
hello world!
```
]

#slide(title: [Rustup])[
```
$ rustup install nightly-2024-03-01
info: syncing channel updates for
      'nightly-2024-03-01-x86_64-unknown-linux-gnu'
info: latest update on 2024-03-01, rust version
      1.78.0-nightly (878c8a2a6 2024-02-29)
info: downloading component 'cargo'
info: downloading component 'clippy'
info: downloading component 'rust-docs'
info: downloading component 'rust-std'
info: downloading component 'rustc'
info: downloading component 'rustfmt'
info: installing component ...
```
]

#slide(title: [Rustup])[
Lätt att använda andra toolchains

```
$ rustc +nightly --version
rustc 1.79.0-nightly (85e449a32 2024-03-22)

$ rustup override set nightly-2022-11-27
info: override toolchain for '...' set to
      'nightly-2022-11-27-x86_64-unknown-linux-gnu'
$ rustc --version
rustc 1.67.0-nightly (80a96467e 2022-11-26)
```
]

#slide(title: [Rustup])[
Kan också installera komponenter för olika versioner

#pause

Exempel: en custom toolchain (utan förkompilerat standardbibliotek) behöver tillgång till källkoden för `core`
#skip.small-back
```
$ rustup component add rust-src --toolchain nightly-2022-11-27
```

#pause

Exempel: ett projekt behöver utvecklas mot 1.48 pga Debian (true story),\ men du vill ha tillgång till LSP
#skip.small-back
```
$ rustup component add rust-analyzer --toolchain 1.48
```
]

#slide(title: [Förhållande till Linux-distributionen])[
  Arch Linux har paket för:
  - rustup
  - rust (innehåller rustc, cargo), senaste stable
  - rust-analyzer

  #pause
  Alpine Linux är ganska likt Arch Linux, fast delade paket för rustc och Cargo.

  #pause
  Debian och Ubuntu har paket för rustc, cargo och några verktyg (inte
  rust-analyzer). Nya versioner kommer ha rustup.

  #pause
  #skip.large

  #grid(columns: (1fr, 1fr), align(center, image(width: 80%, "../choices.jpg")), [
    #pause
    Använd rustup
  ])
]
