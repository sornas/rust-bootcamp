#import "../theme.typ": *
#show: theme

#section[Generics]

#slide(title: [Generics i C++])[
Exempel från TDDD38

```cpp
template <typename T>
T sum(T (&array)[3]) {
  T result{}; // default-initialization
  for (int i{0}; i < 3; ++i) {
    result += array[i];
  }
  return result;
}
```

#pause
#skip.large

#grid(columns: (1fr, 1fr), [
```rust
fn sum<T>(array: &[T; 3]) -> T {
  let mut result = T::default();
  for i in 0..3 {
    result += array[i];
  }
  result
}
```
], [
```rust
fn main() {
  let arr1 = [5, 5, 5];
  let arr2 = [1.05, 1.05, 1.04];
  println!("{}", sum(&arr1));
  println!("{}", sum(&arr2));
}
```
])
]

#slide[
```
error[E0599]: no function or associated item named `default` found
              for type parameter `T` in the current scope
 --> sum.rs:2:25
  |
1 | fn sum<T>(array: &[T; 3]) -> T {
  |        - function or associated item `default` not found
             for this type parameter
2 |     let mut result = T::default();
  |                         ^^^^^^^ function or associated item
                                    not found in `T`
  |
  = help: items from traits can only be used if the type parameter
          is bounded by the trait
help: the following trait defines an item `default`, perhaps
      you need to restrict type parameter `T` with it:
  |
1 | fn sum<T: Default>(array: &[T; 3]) -> T {
  |         +++++++++
```
]

#slide(
  title: [Trait bounds],
)[
För att använda metoder måste vi ge typen en trait bound, ett krav att `T`
implementerar en viss trait

```rust
fn sum<T: Default>(array: &[T; 3]) -> T {
    let mut result = T::default();
    for i in 0..3 {
        result += array[i];
    }
    result
}
```
]

#slide[
```
error[E0368]: binary assignment operation `+=` cannot be applied to type `T`
 --> sum.rs:4:9
  |
4 |         result += array[i];
  |         ------^^^^^^^^^^^^
  |         |
  |         cannot use `+=` on type `T`
  |
help: consider further restricting this bound
  |
1 | fn sum<T: Default + std::ops::AddAssign>(array: &[T; 3]) -> T {
  |                   +++++++++++++++++++++

```
]

#slide[
För att använda metoder måste vi ge typen en trait bound, ett krav att `T`
implementerar en viss trait

```rust
fn sum<T: Default + std::ops::AddAssign>(array: &[T; 3]) -> T {
    let mut result = T::default();
    for i in 0..3 {
        result += array[i];
    }
    result
}
```
]

#slide[
```
error[E0508]: cannot move out of type `[T; 3]`, a non-copy array
 --> sum.rs:4:19
  |
4 |         result += array[i];
  |                   ^^^^^^^^
  |                   |
  |                   cannot move out of here
  |                   move occurs because `array[_]` has type
                      `T`, which does not implement the `Copy` trait
```

#pause
Ingen `help`, men vi lägger till `+ Copy` för vi kan extrapolera
]

#slide[
```rust
fn sum<T: Default + std::ops::AddAssign + Copy>(array: &[T; 3]) -> T {
    let mut result = T::default();
    for i in 0..3 {
        result += array[i];
    }
    result
}

fn main() {
  let arr1 = [5, 5, 5];
  let arr2 = [1.05, 1.05, 1.04];
  println!("{}", sum(&arr1));
  println!("{}", sum(&arr2));
}

15
3.14
```
]

#slide(
  title: [Borrow checker interlude],
)[
Varför behöver vi `Copy`?

#pause
`array[i]` försöker _ta ägande_ över värdet på plats `i`, men `array` är bara en
`&`

Med `copy` gör vi automatiskt en kopiering
]

#slide(title: [Generics i items])[
Items kan innehålla generics

```rust
struct Loc<T> {
  inner: T,    // some thing
  span: Span,  // the location of it in source code
}
```

#pause
#skip.small

```rust
enum Option<T> {
  Some(T),
  None,
}

struct Vec<T> {
  data: *mut T,
  len: usize,
  capacity: usize,
}
```
]

#slide(title: [Generics i items kan ha trait bounds])[
Vi gör en egen Option
```rust
enum Option2<T: Clone> {
  Some(T),
  None,
}
```

Den här kan vi bara stoppa in Clone-saker i.
#pause

```rust
let a1 = Option2::Some(123);
```

#pause
#skip.small
```
struct S {}

let a2 = Option2::Some(S {});
```

```
error[E0277]: the trait bound `S: Clone` is not satisfied
  --> option2.rs:10:28
   |
10 |     let a2 = Option2::Some(S {});
   |              ------------- ^^^^ the trait `Clone` is not
                                      implemented for `S`
   |              |
   |              required by a bound introduced by this call

```
]

#slide(title: [Generics kan göra impl-block conditional])[
```rust
enum Option3<T> {
  Some(T),
  None,
}

impl<T> Option3<T> {
  // functions here can always be called, regardless of T
}

impl<T: Clone> Option3<T> {
  fn clone3(&self) -> Self {
    match self {
      Option3::Some(inner) => Option3::Some(inner.clone()),
      //                                    ^^^^^^^^^^^^^
      Option3::None => Option3::None,
    }
  }
}
```
]
