#import "../theme.typ": *
#show: theme

#section[Referenser]

#slide(
  title: [Vad är en referens?],
)[
  #line-by-line[

    - C++: "an alias to an already-existing object or function."\ (cppreference.com, "Reference
      declaration")
    - Java: Det mesta är referenser, förutom primitiva typer (int/float, boolean)
    - Python: Allt är referenser
  ]

]

#slide(
  title: [Vad är en referens?],
)[
Några skillnader mellan pekare och referens (i C++): \ (`https://stackoverflow.com/a/57492`)
#skip.small

#line-by-line(
  start: 2,
)[
- Pekare kan pekas om, referenser refererar alltid till samma variabel
- Pekare har en storlek (`sizeof`), referenser är transparenta\ `sizeof(&T) == sizeof(T)` (typ)
- Pekare till pekare är OK, pekare till referens är nonsens
- Array av pekare är OK, array av referenser är nonsens
- `**int` är distinkt från `*int`, `&&int` är ekvivalent med `&int`
- Pekare kan vara `nullptr`, en referens som refererar till `NULL` är UB\ (dvs:
  unfathomably based)
  - Referens till nullpekare är OK
- ...
]
]

#slide(title: [`const &T` i C++])[
En const reference kan inte muteras:

```cpp
#include <stdio.h>

int increase_it(const int &n) {
  n++;
  return n;
}

int main() {
  int a = 1;
  increase_it(a);
  printf("%d\n", a);
}
```

#pause
#skip.large

```
main.cpp: In function ‘int increase_it(const int&)’:
main.cpp:4:3: error: increment of read-only reference ‘n’
    4 |   n++;
      |   ^
```
]

#slide(title: [`const &T` i C++])[
...unless?

```cpp
#include <stdio.h>

int increase_it(const int &n) {
  int &m = (int &) n;
  m++;
  return n;
}

int main() {
  int a = 1;
  increase_it(a);
  printf("%d\n", a);
}
```

#pause
#skip.large

`2`
]

#slide(title: [Referenser i Rust])[
Referenser i Rust är typ pekare#only("1")[:]#uncover("2-")[...]

```rust
fn main() {
  let a: u32 = 123;
  let a_ref: &u32 = &a;
  let b: u32 = *a_ref + 333;
  println!("{}", b);
}
```

#pause
...men inte riktigt:

```rust
fn main() {
  let a: u32 = 123;
  let a_ref_many: &&&u32 = &&&a
  let a_ref_many2: &&&&u32 = &&&&a;
  println!("{}",  a_ref_many == a_ref_many2); // :(
  println!("{}", a_ref_many); // :) --> 123
}
```
]

#slide(
  title: [De-referering],
)[
Precis som i C++ behöver vi de-referera en referens för att komma åt värdet

```cpp
void increase_it(int *a) {
  a++;
  *a++;
}

int main() {
  int a = 1;
  increase_it(&a);
}
```

#skip.small

```rust
fn increase_it(n: &i32) {
  *n += 1;
}

fn main() {
  let a = 1;
  increase_it(&a);
}
```
]

#slide(title: [Muterbara referenser])[
```rust
fn increase_it(n: &i32) {
  *n += 1;
}

fn main() {
  let a = 1;
  increase_it(&a);
}
```

#pause
#skip.small

```
error[E0594]: cannot assign to `*n`, which is behind a `&` reference
 --> increase_it.rs:2:3
  |
2 |   *n += 1;
  |   ^^^^^^^ `n` is a `&` reference, so the data it refers to
              cannot be written
  |
help: consider changing this to be a mutable reference
  |
1 | fn increase_it(n: &mut i32) {
  |                    +++

```
]

#slide(
  title: [`&mut`: vi gör väl det då],
)[
```rust
fn increase_it(n: &mut i32) {
  *n += 1;
}

fn main() {
  let a = 1;
  increase_it(&mut a);
}
```

#pause
#skip.large

```
error[E0596]: cannot borrow `a` as mutable, as it is not declared as mutable
 --> increase_it.rs:7:15
  |
7 |   increase_it(&mut a);
  |               ^^^^^^ cannot borrow as mutable
  |
help: consider changing this to be mutable
  |
6 |   let mut a = 1;
  |       +++
```
]

#slide(title: [Okej `let mut` också])[
```rust
fn increase_it(n: &mut i32) {
  *n += 1;
}

fn main() {
  let mut a = 1;
  increase_it(&mut a);
}
```

#pause
#skip.large

```
2
```
]

#slide(
  title: [Skillnaden mellan `mut &T` och `&mut T`],
)[
I Fibonacci-koden skrev vi (bland annat)

```rust
let mut n = 1;
while n > 0 {
  n -= 1;
}
```

#pause
#skip.small

`n -= 1 <=> n = n - 1`

#pause
#skip.large

```rust
let a = 1;
let b = 2;
let mut c = &a;
c = &b;
```

#pause
#skip.large

`mut` är inte en del av typen! `c` har typen `&i32` och är en mut-binding.
]

#slide(title: [Referens till referens])[
Kan den här funktionen mutera den underliggande siffran? \
`fn foo(bar: &mut &i32)`

#pause
#skip.small

Kan den här?\
`fn qux(pec: &&mut i32)`

#pause
#skip.small

Vad _kan_ `foo` göra?
]

#slide(
  title: [uhhhhhhhhhhh],
)[
  Är det här relevant?

  #pause

  Hur ofta manipulerar du en dubbelpekare i C++ (förutom array av arrayer)?
]

#slide(
  title: [Dags att dumma sig],
)[
```rust
fn increase_it(n: &i32) {
  let n: &mut i32 = unsafe { std::mem::transmute(n) };
  *n += 1;
}

fn main() {
  let a = 1;
  increase_it(&a);
  println!("{}", a);
}
```

#pause
#skip.large

```
error: transmuting &T to &mut T is undefined behavior, even if the reference is unused, consider instead using an UnsafeCell
 --> rust-ref-mutref.rs:2:32
  |
2 |     let n: &mut i32 = unsafe { std::mem::transmute(n) };
  |                                ^^^^^^^^^^^^^^^^^^^
  |
  = note: `#[deny(mutable_transmutes)]` on by default
```
]

#slide[
  #v(1fr)
  #align(center, image(width: 40%, "../1984.png"))
  #v(3fr)
]

#slide(title: [Fuck din UB])[
```rust
#![allow(mutable_transmutes)]

fn increase_it(n: &i32) {
  let n: &mut i32 = unsafe { std::mem::transmute(n) };
  *n += 1;
}

fn main() {
  let a = 1;
  increase_it(&a);
  println!("{}", a);
}
```

#pause
#skip.large

```
2
```

#pause
#skip.large

Men vi behövde kämpa för vår rätt att "just flip some bits bro".
]
