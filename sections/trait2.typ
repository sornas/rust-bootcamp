#import "../theme.typ": *
#show: theme

#section[Traits II]

#slide(title: [Egna traits])[
```rust
trait Printable {
  fn print_it(&self);
}

struct Person {
  name: String,
  age: i32,
}

impl Printable for Person {
  fn print_it(&self) {
    println!("{} ({} år)", self.name, self.age);
  }
}

fn print_something<T: Printable>(something: &T) {
  something.print_it();
}
```
]

#slide(title: [Generics])[
```rust
trait CanBecome<T> {
  fn become_it(&self) -> T;
}

struct FunkyNumber {
  foo: u32,
}

impl CanBecome<u32> for FunkyNumber {
  fn become_it(&self) -> u32 {
    self.foo
  }
}

impl CanBecome<u64> for FunkyNumber {
  fn become_it(&self) -> u64 {
    self.foo as u64
  }
}
```
]

#slide(title: [Associated items])[
```rust
trait BecomeSomething {
  type It;

  fn become_it(&self) -> Self::It;
}

impl BecomeSomething for FunkyNumber {
  type It = u32;

  fn become_it(&self) -> u32 {
    self.foo
  }
}
```

Nu kan `FunkyNumber` _bara_ bli en `u32` (genom `BecomeSomething`)
]
