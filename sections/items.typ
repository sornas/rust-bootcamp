#import "../theme.typ": *
#show: theme

#section[Items]

#slide(title: [Struct])[
```rust
struct Person {
  name: String,
  age: i32,
  children: Vec<Person>,
}
```

Lista av fält, inget oväntat

#pause
```rust
let me = Person {
  name: "Gustav Sörnäs".to_string(),
  age: 23,
  children: vec![],
};
```
]

#slide(title: [Enum])[
```rust
enum Color {
  Red,
  Green,
  Blue,
}
```

Instanser av ett enum är en av _varianterna_

#pause
```rust
let my_favorite_color = Color::Red;
```

#pause
#skip.large
Varianter kan ha associerad data

```rust
enum Color {
  Red,
  Green,
  Blue,
  Rgb(u8, u8, u8),
}
```

#pause
```rust
let color1 = Color::Red;
let color2 = Color::Rgb(100, 0, 80);
```
]

#slide(title: [`match`])[
  Vi använder `match` för att "läsa" ett enum

```rust

enum Color {
  Red,
  Green,
  Blue,
  Rgb(u8, u8, u8),
}

fn print_it(color: &Color) {
  match color {
    Color::Red => println!("255-0-0"),
    Color::Green => println!("0-255-0"),
    Color::Blue => println!("0-0-255"),
    Color::Rgb(r, g, b) => println!("{}-{}-{}", r, g, b),
  }
}
```

]

#slide(title: [Metoder])[
```rust
struct Person {
  name: String,
  age: i32,
  children: Vec<Person>,
}

impl Person {
  fn happy_birthday(&mut self) {
    *self.age += 1;
  }
}
```

#pause
#skip.large

```rust
let mut me = Person { ... };
me.happy_birthday();
assert!(me.age == 24);
```

#pause
#skip.large
`&mut self` är samma sak som `self: &mut Self`

]
