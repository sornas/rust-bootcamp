#import "../theme.typ": *
#show: theme

#section[`Vec` och `Option`][Ett smakprov]

#slide(
  title: [`Vec`],
)[
Dynamisk lista av värden (av samma typ)

#pause
Skrivs `Vec<T>` där `T` är någon typ. `Vec<u32>`, `Vec<Vec<Vec<f32>>>`, ...

#pause
Kan skapas på olika sätt:
- `Vec::new()` följt av ett antal `v.push(123)`,\ `v.extend_from_slice(&[1, 1, 2, 3])` osv
- `vec![]` (ett makro): `vec![1, 2, 3, 4, 0, 0, 0]`\
  (också `vec![0; 100]` för att få 100 stycken 0:or)
- `.collect()` på en iterator

#pause
Andra typer av listor:

- Tupler: `(bool, u32)` - fix antal, men kan ha olika typer inuti
- Array: `[f32; 3]` - fix antal av samma typ\ (mindre overhead än en `Vec` och kan
  konstrueras `const`)
]

#slide(
  title: [`Option`],
)[
Antingen `Some` + ett värde, eller `None` utan ett värde

(Explicit `None`/`null`)

#pause
`Option<u32>` har värden som t.ex. `Some(3)`, `Some(0)` och `None`

#pause
Många olika sätt att titta på innehållet:

#let example-1 = [
```rust
if o.is_some() {
  println!("was Some({})", o.unwrap());
} else {
  println!("was None");
}
```
]

#let example-2 = [
```rust
if let Some(n) = o {
  println!("was Some({})", n);
} else {
  println!("was None");
}
```
]

- `o.unwrap()` - plocka ut värdet, panic om `o` är `None`
  #line-by-line(start: 4)[
    - #example-1
    - #example-2
  ]
#only(
  "6-",
)[- `o.unwrap_or(v)` - plocka ut värdet, eller `v` om `o` är `None` (aldrig panic)]
]
