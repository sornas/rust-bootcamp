#import "../theme.typ": *
#show: theme

#section[`Iterator`]

#slide(
  title: [Vad är en iterator?],
)[
#pause

> In computer programming, an iterator is an object that enables a programmer to
traverse a container, particularly lists.

#align(right)["Iterator", `en.wikipedia.org`]

#pause
Olika modeller i olika språk:

#line-by-line(
  start: 3,
)[
- Python: " The iterator provides a 'get next' value operation that produces the
  next item in the sequence each time it is called, raising an exception when no
  more items are available."\ (`https://peps.python.org/pep-0234/`)
- C++: "Iterators are a generalization of pointers that allow a C++ program to
  work with different data structures (for example, containers and ranges) in a
  uniform manner."\ (`https://en.cppreference.com/w/cpp/iterator`)
- C: #only("6-")[#ferris.wtf]
#pause
]
]

#slide(title: [Iteratorer i Rust])[
```rust
trait Iterator {
  type Item;

  fn next(&mut self) -> Option<Self::Item>;
}
```

#box(image(height: 2em, "../CLEAN.png"))
#box(image(height: 2em, "../CLEAN.png"))
#box(image(height: 2em, "../CLEAN.png"))
]

#slide(title: [`impl<T> Iterator for Vec<T>`])
