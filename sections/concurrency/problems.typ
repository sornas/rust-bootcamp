#import "../../theme.typ": *
#show: theme

#section[Utmaningar med concurrency]

#slide[
  _Operating Systems: Three Easy Pieces_, (`https://www.ostep.org/`) går igenom olika typer av concurrency-buggar.

  - Deadlock
  - Atomicity-Violation Bugs
  - Order-Violation Bugs
]

#slide()[
  #align(center, image("../../three-easy-pieces.jpg"))
]

#slide(title: [Deadlock])[
  Två tasks väntar på varandra på något sätt.
]

#slide(title: [Atomicity-Violation])[]
