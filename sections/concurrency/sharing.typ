#import "../../theme.typ": *
#show: theme

#section[Kommunikation i concurrency]

#slide[
  (Jag kommer säga task, för det är typ samma problem och lösningar.)

  I både trådar och async såg vi argument / retur-värde för att kommunicera värden. Vad gör vi om två tasks vill prata med varandra medan dom kör?

  #pause

  Två huvudsakliga metoder:
  - Delat minne
  - Message passing

  #pause
]

#slide(title: [Delat minne])[
Trådar delar en del minne, så dom kan turas om att läsa/skriva värden.

#pause

```cpp
int n = 0;
void t1() {
  n += 5;
}
void t2() {
  n += 5;
  cout << n << endl;
}

int main() {
  auto thread1 = std::thread(t1);
  auto thread2 = std::thread(t2);
  thread1.join();
  thread2.join();
}
```

#pause
#skip.small

```
$ g++ thread.cpp; ./a.out
10
```

#pause
```
:) <-- person som inte vet att hon precis körde UB
```
]

#slide(title: [Delat minne i Rust])[
```rust
fn main() {
  let mut n = 0;
  std::thread::spawn(move || {
    n += 5;
  });
  std::thread::spawn(move || {
    println!("{n}");
  });
}
```

#skip.small
#pause

```
0
```
]


#slide[
```rust
fn main() {
    let mut s = String::new();
    let t1 = std::thread::spawn(move || {
        s.push_str("5")
    });
    let t2 = std::thread::spawn(move || {
        println!("{}", s);
    });
    t1.join().unwrap();
    t2.join().unwrap();
}
```
]

#slide[
```
error[E0382]: use of moved value: `s`
 --> shared-memory.rs:6:33
  |
2 |     let mut s = String::new();
  |         ----- move occurs because `s` has type `String`, which
                  does not implement the `Copy` trait
3 |     let t1 = std::thread::spawn(move || {
  |                                 ------- value moved into closure
                                            here
4 |         s.push_str("5")
  |         - variable moved due to use in closure
5 |     });
6 |     let t2 = std::thread::spawn(move || {
  |                                 ^^^^^^^ value used here after move
7 |         println!("{}", s);
  |                        - use occurs due to use in closure
```
]

#slide[
  Trådar vill äga sina värden (och kan inte ta referenser).

  #pause
  Vi kan använda en smart pekare, i det här fallet en referens-räknad pekare.

  Förenklat:
  #skip.small-back

```rust
struct Rc<T> {
  strong: usize,
  weak: usize,
  value: *mut T,
}
```

- `Rc::new(0i32)`: allokerar på heapen och sparar en pekare till allokeringen i en ny `Rc`
- `rc.deref()` (samma sak som `*rc`): ger en `&T`

#skip.large
#pause

Okej, men vi vill mutera också.
]

#slide(title: [Interior mutability])[
  Det finns några olika typer av interior mutability, som ni troligen stött på tidigare.

  #line-by-line[

  - Mutex: mutual exclusion. Endast en i taget kan komma åt ett värde. Samma sak som ett lås.
  - RwLock: reader-writer lock. Antingen 1+ läsare, eller exakt 1 skrivare. \ Läsare är `&T`, skrivare är `&mut T`.
  ]

  #uncover("4-")[
    Interior mutability innebär att vi går från `&Mutex<T>` till `&mut T`.
  ]

  #uncover("5-")[
  Vi har några andra synkroniseringsprimitiver också.
  ]

  #uncover("6-")[- Barrier: se till att flera trådar har nått en programposition. Blockerar tills ett visst antal trådar väntar på samma barriär.]
  #uncover("7-")[- Condvar: condition variable. Kombineras med en Mutex. (???)]
]

#slide(title: [Interior mutability])[

```rust
use std::{rc::Rc, sync::Mutex};
fn main() {
    let n = Rc::new(Mutex::new(0i32));
    let t1 = std::thread::spawn(move || {
        *n.lock().unwrap() += 5;
    });
    let t2 = std::thread::spawn(move || {
        println!("{}", n.lock().unwrap());
    });
    t1.join().unwrap();
    t2.join().unwrap();
}
```
]

#slide(title: [Du trodde!! Gottem])[
```
error[E0277]: `Rc<Mutex<i32>>` cannot be sent between threads safely
   --> shared-memory.rs:4:33
    |
4   |       let t1 = std::thread::spawn(move || {
    |                ------------------ ^------
    |                |                  |
    |  ______________|__________________within this
      |     `{closure@shared-memory.rs:4:33: 4:40}`
    | |              |
    | |              required by a bound introduced by this call
    | |
5   | |         *n.lock().unwrap() += 5;
6   | |     });
    | |_____^ `Rc<Mutex<i32>>` cannot be sent between threads safely
```
]

#slide(title: ["T cannot be sent between thread safely"])[
  #v(1fr)
  #align(center, text(size: 1.5em)[Rust-polisens mest frustrerande felmeddelande??])
  #v(2fr)
]

#slide(title: [])[
  Okej det var inte så illa

  #align(center, image("../../rc-single-thread.png"))

  Det står ju faktiskt där #text(size: 1.5em, ferris.gentleman)
]

#slide(title: [])[
  Vi vill använda en `Arc` istället ("Atomically Reference Counted"):

```rust
use std::{sync::{Arc, Mutex}};
fn main() {
  let n = Arc::new(Mutex::new(0i32));
  let n1 = Arc::clone(&n);  // <-- kan inte flytta n
  let t1 = std::thread::spawn(move || {
    *n1.lock().unwrap() += 5;
  });
  let n2 = Arc::clone(&n);  // <--
  let t2 = std::thread::spawn(move || {
    println!("{}", n2.lock().unwrap());
  });
  t1.join().unwrap();
  t2.join().unwrap();
}
```

#pause
#skip.large

```
$ ./shared-memory
0
$ ./shared-memory
5
```
]

#slide(title: [])[
$=>$ Kompilatorn hjälper dig med några problem, men _inte alla_.
]

#slide(title: [Send och Sync])[
  Hur vet kompilatorn att saker inte kan skickas mellan trådar?

  #pause
  - `Send`: "Types that can be transferred across thread boundaries"
  - `Sync`: "Types for which it is safe to share references between threads"
#skip.small
  - `T: Send` $=>$ `T` kan skickas mellan trådar
  - `T: Sync` $=>$ `&T` kan skickas mellan trådar

  Send och Sync är auto-traits (några av dom få) vilket betyder att kompilatorn lägger på dom automatiskt om det är giltigt.

  #pause
  Oftast behöver du inte bry dig om `Send/Sync` i sig, utan lyssna på vad kompilatorn försöker säger.

  #pause
  `Arc<Mutex<T>>` är en riktig cheat-code för att göra vad som helst `Send + Sync`, men du får ju också en Mutex att handskas med.

]

#slide(title: [Kort om async])[
  `std::sync` är blockerande, så i async-kod behöver du async-vänlig synkronisering.

  - `smol` har `smol::lock::{Mutex, RwLock, Barrier, Semaphore}`
  - `tokio` har `tokio::sync::{Mutex, RwLock, Barrier, Semaphore}`
  - `embassy` har `embassy_sync::{Mutex}` (enbart?)
]

#slide(title: [Message passing])[
  Utöver shared memory har vi message passing. Tasks skickar meddelanden till varandra genom _kanaler_.

#grid(columns: (1fr, auto), [
#pause
#set text(size: 0.8em)
```rust
use std::sync::mpsc::{channel, Receiver, Sender};
fn task1(tx: Sender<u32>) {
    println!("sending 1");
    tx.send(1).unwrap();
    println!("sending 2");
    tx.send(2).unwrap();
    println!("sending 3");
    tx.send(3).unwrap();
    // tx går ur scope
}
fn task2(rx: Receiver<u32>) {
    while let Ok(n) = rx.recv() {
        println!("received {n}");
    }
    println!("nothing more to receive");
}
fn main() {
    let (tx, rx) = channel();
    let t2 = std::thread::spawn(move || task2(rx));
    let t1 = std::thread::spawn(move || task1(tx));
    t1.join().unwrap();
    t2.join().unwrap();
}
```
], [
#pause
Möjlig output:
```
sending 1
received 1
sending 2
sending 3
received 2
received 3
nothing more to receive

```
])
]

#slide(title: [Message passing])[
  `mpsc` står för "multiple producer, single consumer".

  #pause
  På samma sätt som med synkroniseringsprimitiverna finns det async-kanaler som du behöver använda i async, eftersom både skicka och ta emot kan blocka (beroende på implementation; vissa kanaler är unbounded och då kommer `send` aldrig blocka, men `recv` kommer alltid kunna blocka).

  #pause
  - `smol` har `smol::channel` och är en `mpmc` (multiple producer, multiple consumer)
  - `tokio` har `tokio::sync::mpsc` (och lite annat)
]

#slide(title: [Message passing genom stdio])[
  Det finns crates för att skicka meddelanden mellan _processer_.

  Till exempel: `https://lib.rs/crates/os_pipe`

#pause

```rust
let mut command = ...;

let (mut reader, writer) = os_pipe::pipe()?;
let writer_clone = writer.try_clone()?;
command.stdout(writer);
command.stderr(writer_clone);

let mut handle = command.spawn()?;
drop(command);

let mut output = String::new();
reader.read_to_string(&mut output)?;
handle.wait()?;
assert_eq!(output, "foobar");
```
#pause
(Men då bara strängar)

  #pause
  Du kan inte dela minne mellan processer. Typ. (`/proc` är olagligt!!! #text(baseline: 2pt, size: 1.2em, ferris.police))
]

#slide(title: [Atomics])[
  För primitiver kan du ofta använda en atomic istället för att behöva sträcka dig efter en Mutex (om du inte behöver själva låset över flera programrader av någon anledning).

  #pause
  En atomic kan utföra en viss mängd operationer i en enda instruktion:

```
n += 5
=>
load r0, (n)
addi r0, r0, 5
store (n), r0
```

#pause
#skip.small
```rust
let n = AtomicU32::new(5);
let old = n.fetch_add(5, Ordering::SeqCst); // n += 5
```

#pause
hallå vad i helvete är en `Ordering` #text(baseline: 4pt, size: 1.5em, ferris.exorcism)
]

#slide(title: [Abstraktioner på concurrency])[
  Concurrency-modellen i Rust är fortfarande väldig generell.

  Med begränsningar kan vi göra det mycket enklare.

]

#slide[
```rust
let values: Vec<_> = (0..10_000_000).collect();
let results: Vec<_> = values
  .into_iter()
  .map(heavy_computation)
  .collect();
```
]

#slide[
```rust
let values: Vec<_> = (0..10_000_000).collect();
let tasks: Vec<_> = values
  .into_iter()
  .map(|n| std::thread::spawn(|| heavy_computation(n)))
  .collect();
let results: Vec<_> = tasks
  .into_iter()
  .map(|t| t.join().unwrap())
  .collect();
```

#pause

10 miljoner trådar tar nog lite tid att starta.
]

#slide[
`rayon` gör det enkelt.
#skip.small

```rust
use rayon::prelude::*;

let values: Vec<_> = (0..10_000_000).collect();
let results: Vec<_> = values
  .into_par_iter() // <--
  .map(heavy_computation)
  .collect();
```

Tasks hamnar i en kö, och när en worker thread är idle hämtar den en ledig task och kör den.

OBS: om `heavy_computation` inte alls är heavy kan det fortfarande gå snabbare att köra enkeltrådat. Även om du har 8 kärnor / 16 trådar.
]
