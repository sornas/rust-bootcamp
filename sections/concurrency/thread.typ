#import "../../theme.typ": *
#show: theme

#section[`std::thread`]

#slide[
```rust
fn square(n: u32) -> u32 {
  let mut res = 1;
  for _i in 0..n {
    res += n;
  }
  res
}

fn main() {
  for n in 0..10000 {
    println!("{}^2 = {}", n, square(n));
  }
}
```

#pause
#skip.large

```
Executed in  548.78 millis    fish           external
   usr time  525.40 millis    1.11 millis  524.28 millis
   sys time   23.60 millis    0.23 millis   23.38 millis
```
]

#slide[
```rust
fn square(n: u32) -> u32 {
  let mut res = 1;
  for _i in 0..n {
    res += n;
  }
  res
}

fn main() {
  for n in 0..10000 {
    std::thread::spawn(|| {
      println!("{}^2 = {}", n, square(n));
    });
  }
}

```
]

#slide[
```
error[E0373]: closure may outlive the current function, but it borrows
              `n`, which is owned by the current function
  --> thread.rs:11:28
   |
11 |         std::thread::spawn(|| {
   |                            ^^ may outlive borrowed value `n`
12 |             println!("{}^2 = {}", n, square(n));
   |                                   - `n` is borrowed here

```

#skip.small
#pause
```
help: to force the closure to take ownership of `n` (and any other
      referenced variables), use the `move` keyword
   |
11 |         std::thread::spawn(move || {
   |                            ++++

```
]

#slide[
```rust
for n in 0..10000 {
  std::thread::spawn(move || {
    println!("{}^2 = {}", n, square(n));
  });
}
```
]

#slide[
```
...
9985^2 = 99700226
9993^2 = 99860050
9994^2 = 99880037
9992^2 = 99840065
9995^2 = 99900026

________________________________________________________
Executed in  295.00 millis    fish           external
   usr time  447.21 millis    1.65 millis  445.55 millis
   sys time  975.92 millis    0.36 millis  975.56 millis

```

#skip.large
#pause
```
$ ./thread | wc -l
9997
$ ./thread | wc -l
9993
$ ./thread | wc -l
9992
$ ./thread | wc -l
9998
```
]

#slide[
#set text(size: 0.9em)
```rust
fn square(n: u32) -> u32 {
  let mut res = 1;
  for _i in 0..n {
    res += n;
  }
  res
}

fn main() {
  let mut threads = Vec::new(); // trådar som kör
  for n in 0..10000 {
    threads.push((                  // starta en ny tråd. spara:
      n,                            // <-- input
      std::thread::spawn(move || {  // /
        square(n)                   // |-- ett JoinHandle
      }),                           // \
    ));
  }

  for (n, t) in threads {
    println!("{}^2 = {}", n, t.join().unwrap());
  }
}
```
]

#slide[
```
...
9995^2 = 99900026
9996^2 = 99920017
9997^2 = 99940010
9998^2 = 99960005
9999^2 = 99980002

________________________________________________________
Executed in    1.32 secs    fish           external
   usr time    0.19 secs    1.88 millis    0.18 secs
   sys time    1.91 secs    0.45 millis    1.91 secs
```
]

#slide(title: [Threads tar ägande över alla argument som skickas])[

```rust
use std::{fmt::Debug, thread::{spawn, JoinHandle}};

fn stringed(s: &str) -> String {
  s.to_string()
}

fn wait_on_thread<T: Debug>(t: JoinHandle<T>) {
  let value = t.join();
  println!("thread joined with value: {:?}", value);
}

fn main() {
  let join_handle = {
    let s = String::from("i'm a little string");
    spawn(move || stringed(&s))
    // closure-funktionen måste ta ägande över s
  };
  wait_on_thread(join_handle);
}
```
]

#slide(title: [`std::thread::scope`])[
```rust
fn main() {
  let mut a = vec![1, 2, 3];
  let mut x = 0;

  std::thread::scope(|s| {
    // spawna en tråd som _lånar_ ett värde
    s.spawn(|| {
      dbg!(&a);
    });
    dbg!(x);
    // spawna en tråd som _lånar_ ett värde
    s.spawn(|| {
      x += a[0] + a[2];
    });
    // x är lånad tills slutet på 'scope', för rustc vet inte
    // när tråden är klar.
    // dbg!(x);  error: cannot use `x` because it was mutably borrowed
  }); // nu väntar vi på alla trådar
  x *= 2; // x är tillgänglig igen
}
```
]
