#import "../../theme.typ": *
#show: theme

#section[Trådning + Async]

#slide()[
Executors kan vara antingen enkeltrådade eller multitrådade.

```rust
smol::LocalExecutor::spawn<'a, T, F>(future: F)
  where F: Future<Output = T> + 'a
        T: 'a
```

#pause
#skip.small

```rust
smol::Executor::spawn<'a T, F>(future: F)
  where F: Future<Output = T> + Send + 'a
        T: Send + 'a
```
]
