#import "../../theme.typ": *
#show: theme

#section[`async/await`]

#slide(title: [`trait Future`])[
En coroutine är en funktion som returnerar en `Future`:

```rust
trait Future {
  type Output;

  fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>)
    -> Poll<Self::Output>;
}

enum Poll<T> {
    Ready(T),
    Pending,
}
```

#pause
Dvs, vi "pollar" en Future och får antingen "answer unclear, ask again later" eller det färdiga värdet.

Ofta har vi flera coroutines igång samtidigt och pollar i någon ordning.
]

#slide(title: [Executor])[
  En _executor_ ansvarar för att polla Futures. Rust har inte en executor i standardbiblioteket, så du får skriva en själv eller använda en crate.

  #pause
  Enbart executor:
  - `async-executor` / `futures-executor`
  - `embassy-executor`: designad för embedded (!) -- allokeringsfri

  #pause
  Som en del av ett större ramverk:
  - `tokio`: status quo inom async. Multi-threaded runtime.
  - `smol` (innehåller `async-executor`)\ både single-thread och multi-thread runtime
  - `glommio`: bygger på Linux `io_uring`-API
  - `embassy`

  #pause
  Skriv en själv:
  #uncover("5-")[- Nej]
]

#slide(title: [`smol` exempel])[
```rust
use smol::Timer;
use std::time::Duration;

async fn work_it(n: u32) -> u32 {  // -> impl Future<Output = u32>
  Timer::after(Duration::from_secs(1)).await;
  n
}

async fn main2() {
  let n1 = work_it(1).await;
  let n2 = work_it(2).await;
  let n3 = work_it(3).await;

  println!("{n1} {n2} {n3}");
}

fn main() {
  smol::block_on(main2());  // main2 returnerar impl Future<Output=()>
}
```

]

#slide()[
```
$ time cargo run
1 2 3

________________________________________________________
Executed in    3.01 secs      fish           external
   usr time    6.47 millis    1.09 millis    5.39 millis
   sys time    0.27 millis    0.27 millis    0.00 millis
```

3 sekunder ??

#pause

```rust
let n1 = work_it(1).await;
// vänta tills work_it(1) är klar
let n2 = work_it(2).await;
// vänta tills work_it(2) är klar
let n3 = work_it(3).await;
// vänta tills work_it(3) är klar

println!("{n1} {n2} {n3}");
```
]

#slide()[
```rust
// starta tasks
let task1 = work_it(1);
let task2 = work_it(2);
let task3 = work_it(3);
// joina tasks
let n1 = task1.await;
let n2 = task2.await;
let n3 = task3.await;

println!("{n1} {n2} {n3}");
```

#pause
#skip.small

```
$ time target/debug/executors
1 2 3

________________________________________________________
Executed in    3.01 secs      fish           external
   usr time    3.16 millis  816.00 micros    2.35 millis
   sys time    3.47 millis  290.00 micros    3.18 millis
```

#ferris.grimace
]

#slide()[
```rust
// starta tasks
let task1 = smol::spawn(work_it(1));
let task2 = smol::spawn(work_it(2));
let task3 = smol::spawn(work_it(3));
// joina tasks
let n1 = task1.await;
let n2 = task2.await;
let n3 = task3.await;

println!("{n1} {n2} {n3}");
```

#pause
#skip.small

```
$ time target/debug/executors
1 2 3

________________________________________________________
Executed in    1.01 secs      fish           external
   usr time    0.93 millis  933.00 micros    0.00 millis
   sys time    6.25 millis  319.00 micros    5.93 millis
```
]

#slide(title: [Blanda sync/async])[
```rust
async fn work_it(n: u32) -> u32 {
  std::thread::sleep(Duration::from_secs(1));
  n
}
```

#pause
#skip.small

```
$ time target/debug/executors
1 2 3

________________________________________________________
Executed in    3.01 secs      fish           external
   usr time    4.42 millis  814.00 micros    3.61 millis
   sys time    0.31 millis  308.00 micros    0.00 millis

```

#pause

Behöver använda async-versioner av vanliga funktioner.
- sleep
- filsystem
- nätverk
- ...

Samlade i tokio/smol\ ...men vad händer om koden är skriven i ett bibliotek?
]

#slide(title: [Blanda sync/async])[
#set text(size: 0.9em)
```rust
use std::{path::Path, time::Duration};
use glob::glob;

fn calculate(path: &Path) {
  std::thread::sleep(Duration::from_micros(1));
  println!("{:?}", path.display());
}

fn lib() {
  for entry in glob("media/*.*").unwrap() {
    match entry {
      Ok(path) => calculate(&path),
      Err(e) => println!("{:?}", e),
    }
  }
}

async fn main2() {
  lib();
}

fn main() { smol::block_on(main2()); }

```
]

#slide()[
```
$ time target/debug/async-glob
...
________________________________________________________
Executed in    1.51 secs      fish           external
   usr time  216.34 millis    0.00 millis  216.34 millis
   sys time  157.75 millis    1.09 millis  156.66 millis
```

#pause
#skip.large

```rust
async fn main2() {
  lib();
  lib();
}
```

#pause

```
________________________________________________________
Executed in    2.99 secs      fish           external
   usr time  375.37 millis    0.00 millis  375.37 millis
   sys time  366.23 millis    1.01 millis  365.22 millis
```
]

#slide[

```rust
let t1 = smol::spawn(|| lib());
let t2 = smol::spawn(|| lib());
t1.await;
t2.await;
```

#pause
#skip.large

```
error[E0277]: `{closure@src/main.rs:21:26: 21:28}` is not a future
  --> src/main.rs:21:26
   |
21 |     let t1 = smol::spawn(|| lib());
   |              ----------- ^^^^^^^^
                              `{closure@src/main.rs:21:26: 21:28}`
                              is not a future
   |              |
   |              required by a bound introduced by this call
   |
   = help: the trait `Future` is not implemented for closure
           `{closure@src/main.rs:21:26: 21:28}`
   = note: {closure@src/main.rs:21:26: 21:28} must be a future
            or must implement `IntoFuture` to be awaited
```
]

#slide()[
```rust
async fn main2() {
  let t1 = smol::spawn(smol::unblock(|| lib()));
  let t2 = smol::spawn(smol::unblock(|| lib()));
  t1.await;
  t2.await;
}
```

#pause

```
________________________________________________________
Executed in    1.51 secs      fish           external
   usr time  390.95 millis    0.00 micros  390.95 millis
   sys time  356.27 millis  861.00 micros  355.41 millis

```
]
