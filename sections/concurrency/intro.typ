
#import "../../theme.typ": *
#show: theme

#section[Introduktion till concurrency]

#slide(title: [Vad är concurrency?])[
  #pause
  "In computer science, concurrency is the ability of different parts or units of a program, algorithm, or problem to be executed out-of-order or in partial order, without affecting the outcome."\ `https://en.wikipedia.org/wiki/Concurrency_(computer_science)`
  #pause

  Vad är parallellism?
  #pause

  "The term Parallelism refers to techniques to make programs faster by performing several computations at the same time."\ `https://wiki.haskell.org/Parallelism_vs._Concurrency`
  #pause

  Parallellism är _en_ möjlig metod för concurrency.
]

#slide(title: [Typer av concurrency])[
  Det mest grundläggande är att starta en ny process/tråd.

  #pause
  På syscall-nivå:

  - `fork(3p)` sedan urminnes tider (tidigast 1962) \ Skapar en ny process genom att duplicera den nuvarande
  - På Linux: `clone(2)` med `CLONE_THREAD` sedan Linux 2.4 (2001) \ Skapar en ny tråd (fast egentligen är det en ny process, shh säg inget!)

  #pause
  Ofta har ditt valda programmeringsspråk en abstraktion för att starta trådar så du slipper använda `fork`/`clone`.

  - C: `pthreads.h`
  - C++11: `std::thread`
  - Java: `java.lang.Thread`
  - Python: `thread` och `multiprocessing`

  Gemensamt är att alla har API där du skickar en funktion som argument och börjar köra den funktionen på en ny tråd.
]

#slide(title: [Typer av concurrency])[
  En coroutine är en subrutin ("funktion") som kan pausa och återuppta exekvering.

  #pause
  Från början (1960-talet) en metod för assembly-programmering.
  Lever vidare som t.ex. `async/await` i JavaScript, Python, ... och Rust såklart.

  #pause
  Corutines möjliggör out-of-order execution på samma tråd.

  #pause
  Trådar är mer av en library feature, medan coroutines är en language feature.
]

#slide(title: [Concurrency i Rust])[
  Vi ser att Rust har stöd för både trådar och coroutines. Vad är skillnaden?

  #pause
  Trådar är balls-to-the-walls parallellism.

  Nackdelar:
  1. overhead när trådar skapas
  2. overhead när trådar ska kommunicera

  #pause
  Passar bra när programmet är CPU-begränsat och enkelt delas upp i separata delar.

  #skip.large

  #pause
  Coroutines är bättre när programmet är IO-begränsat, t.ex. ska öppna och läsa några tusen filer.

  Nackdelar:
  1. overhead när vi byter mellan coroutines
  2. finns ingen default runtime
  3. delar upp funktioner i delvis inkompatibla async/sync ("function coloring")
]
