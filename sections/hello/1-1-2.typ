#import "../../theme.typ": *
#show: theme

#section([Räkna med 1 + 1 = 2])

#slide()[
```rust
fn main() {
  let a = 1;
  println!("{}", a);
}
```

#pause

```shell
$ cargo run
  Compiling ...
    Running `target/debug/hello-world`
1
```
]

#slide(
  )[
```rust
fn main() {
  let a = 1;
  a += 1;
  println!("{}", a);
}
```

#pause

#text(size: 1.5em)[
  #ferris.police
  #ferris.police
  #ferris.police
]

#[
#set text(size: 0.8em)
```shell
$ cargo run
error[E0384]: cannot assign twice to immutable variable `a`
 --> src/main.rs:3:5
  |
2 |     let a = 1;
  |         -
  |         |
  |         first assignment to `a`
  |         help: consider making this binding mutable: `mut a`
3 |     a += 1;
  |     ^^^^^^ cannot assign twice to immutable variable

For more information about this error, try `rustc --explain E0384`.
error: could not compile `hello-world` (bin "hello-world") due to 1 previous error
```
]
]

#slide(
  title: [`let` / `const`],
)[

I C et al är variabler mutable-by-default, och `const` gör dom immutable

#pause
#skip.small

```c
int main() {
  const int a = 1;
  a += 1;
}
```

#pause

#[
#set text(size: 0.8em)
```
$ clang let.c
let.c:3:5: error: cannot assign to variable 'a' with const-qualified type 'const int'
    3 |   a += 1;
      |   ~ ^
let.c:3:13: note: variable 'a' declared const here
    2 |   const int a = 1;
      |   ~~~~~~~~~~^~~~~
1 error generated.
```
]

#pause
#skip.small

Slutsats: `let a = 1;` motsvarar ungefär `const int a = 1;` \
]

#slide(title: [`let mut`])[
Från tidigare felmeddelandet:\
`help: consider making this binding mutable: 'mut a'`

#pause

```rust
fn main() {
    let mut a = 1;
    //  ^^^
    a += 1;
    println!("{}", a);
}
```

#pause

```shell
$ cargo run
2
```

#ferris.heart-eyes

]
