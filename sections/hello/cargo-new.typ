#import "../../theme.typ": *
#show: theme

#section([Hello World])

#slide(title: [Baby steps])[
```sh
$ sudo pacman -S rust
$ cargo new hello-world
  Created binary (application) `hello-world` package
$ cd hello-world
$ tree
.
├── Cargo.toml
└── src
    └── main.rs
```

#pause
#skip.small

```sh
$ cargo run
Compiling hello-world v0.1.0 (.../hello-world)
 Finished dev [unoptimized + debuginfo] target(s) in 0.35s
  Running `target/debug/hello-world`
Hello, world!
```
]

#slide(
  title: [`Cargo.toml`],
)[
#[
#set text(size: 1em)
```toml
[package]
name = "hello-world"
version = "0.1.0"
edition = "2021"

[dependencies]
# empty
```
]

- `name = "hello-world"`\ Programmets namn.\ `$ ./target/debug/hello-world`

- `version = "0.1.0"`\ Programmets version
- `edition = "2021"`\ Vilken Rust-edition vi kompilerar med
- `[dependencies]`\ Lista av dependencies
]

#slide(
  title: [`src/main.rs`],
)[
#[
#set text(size: 1em)
```rust
fn main() {
  println!("Hello, world!");
}
```
]

- `fn main() { .. }`\ En funktion som heter `main` utan argument och return-typ.

- `println!`\ Ett makro som skriver ut text följt av `\n` på `stdout`.
  - Python: `print("Hello, world!")`
  - C: `printf("Hello, world!");`
  - C++: `cout << "Hello, world!" << endl;`
]

#slide(title: [Ett makro? Redan? #ferris.exorcism])[

Rust-funktioner har inte variadic arguments
#pause

C: #skip.small-back

```c
int a = 10;
printf("%d/%d\n", 1, a);
```

#pause#skip.tiny

Rust: #skip.small-back
```rust
let a = 10;
println!("{}/{}", 1, a);
```

// #pause#skip.tiny
//
// Utan makro: #skip.small-back
// ```rust
// use std::io::{stdout, Write};
// fn main() {
//   let mut buf = String::new();
//   let a = 10;
//   buf.push_str(1.to_string().as_str());
//   buf.push_str("/");
//   buf.push_str(a.to_string().as_str());
//   stdout().write(buf.as_bytes()).unwrap();
// }
// ```

]
