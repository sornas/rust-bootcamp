#import "../../theme.typ": *
#show: theme

#section[Några ord om typer]

#slide()[
```rust
fn main() {
  let mut n = 10;
  let mut a = 0;
  let mut b = 1;
  let mut F = vec![0, 1];

  while n > 0 {
    a += b;
    F.push(a);
    let swap = a;
    a = b;
    b = swap;
    n -= 1;
  }

  println!("{}", std::any::type_name_of_val(&F));
}
```

#pause
#skip.large
```
alloc::vec:Vec<i32>
```
]

#slide()[
```rust
fn main() {
  let mut n = 10;
  let mut a = 0;
  let mut b = 1;
  let mut F: Vec<u32> = vec![0, 1];

  while n > 0 {
    a += b;
    F.push(a);
    let swap = a;
    a = b;
    b = swap;
    n -= 1;
  }

  println!("{}", std::any::type_name_of_val(&F));
}
```

#pause
#skip.large
```
alloc::vec:Vec<u32>
```
]

#slide()[
```rust
fn main() {
  let mut n = 10;
  let mut a: u16 = 0;
  let mut b = 1;
  let mut F = vec![0, 1];

  while n > 0 {
    a += b;
    F.push(a);
    let swap = a;
    a = b;
    b = swap;
    n -= 1;
  }

  println!("{}", std::any::type_name_of_val(&F));
}
```

#pause
#skip.large
```
alloc::vec:Vec<u16>
```
]
#slide()[
```rust
fn main() {
  let mut n = 10;
  let mut a: u16 = 0;
  let mut b: i32 = 1;
  let mut F = vec![0, 1];

  while n > 0 {
    a += b;
    F.push(a);
    let swap = a;
    a = b;
    b = swap;
    n -= 1;
  }

  println!("{}", std::any::type_name_of_val(&F));
}
```
]

#slide()[
```
error[E0277]: cannot add-assign `i32` to `u16`
 --> src/main.rs:8:11
  |
8 |         a += b;
  |           ^^ no implementation for `u16 += i32`
  |
  = help: the trait `AddAssign<i32>` is not implemented for `u16`
  = help: the following other types implement trait `AddAssign<Rhs>`:
            <u16 as AddAssign>
            <u16 as AddAssign<&u16>>
```
]
