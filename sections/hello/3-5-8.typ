#import "../../theme.typ": *
#show: theme

#section([Räkna med 3, 5, 8])

#let fibonacci-it = pseudocode(
  no-number,
  [*input:* positive integer $n$],
  no-number,
  [*output:* first $n+2$ fib. numbers],
  [*let* $a <- 0$],
  no-number,
  [#hide[*let*] $b <- 1$],
  no-number,
  [#hide[*let*] $F <- (0, 1)$],
  [*while* $n > 0$],
  ind,
  [$a <- a + b$],
  [`append`($F$, $a$)],
  [`swap`($a$, $b$)],
  [$n <- n - 1$],
  ded,
  [*return* $F$],
)

#slide()[
  Nu ska vi räkna med Fibonacci-tal

  Rekursiv definition:

  $
    F_0 = 0, F_1 = 1 \
    F_n = F_(n-1) + F_(n-2) \
    F = 0, 1, 1, 2, 3, 5, 8, ...
  $

  #pause

  Vi börjar med en iterativ algoritm:

  #fibonacci-it
]

#slide(
  )[
  #fibonacci-it

  #only(1)[Hur skriver vi det i Rust?\ Eller i ett språk vi känner igen?]
][
#only(2)[
```python
def main():
  n = 10
  a = 0
  b = 1
  F = [0, 1]

  while n > 0:
    a += b
    F.append(a)
    a, b = b, a
    n -= 1

  print(F)
```
]
]

#slide()[
  #fibonacci-it
][
```rust
fn main() {
  let mut n = 10;
  let mut a = 0;
  let mut b = 1;
  let mut F = vec![0, 1];

  while n > 0 {
    a += b;
    F.push(a);
    let swap = a;
    a = b;
    b = swap;
    n -= 1;
  }

  println!("{:?}", F);
}
```
]
