#import "../../theme.typ": *
#show: theme

#section([Funktioner])

#slide(
  )[
Det sägs att det är dålig kod att skriva allt i `main`. #super[[citation needed]]

#grid(columns: (1.0fr, 1.0fr), [
#pause
```rust
fn fib(n: u32) -> Vec<u32> {
  //   ^^^^^^     ^^^^^^^^
  //   |          |
  //   argument   return-typ
  let mut a = 0;
  let mut b = 1;
  let mut F = vec![0, 1];

  while n > 0 {
    a += b;
    F.push(a);
    let swap = a;
    a = b;
    b = swap;
    n -= 1;
  }

  F // implicit return
}
```
], [
#pause
```rust
fn main() {
  let da_fibs = fib(10);
  println!("{:?}", da_fibs);
}
```

Som man förväntar sig!

#pause

...unless?
])
]

#slide(title: [Argument är immutable-by-default])[

#text(size: 2em)[
  #ferris.police
  #ferris.police
  #ferris.police
]

```
error[E0384]: cannot assign to immutable argument `n`
  --> src/main.rs:12:9
   |
1  | fn fib(n: u32) -> Vec<u32> {
   |        - help: consider making this binding mutable: `mut n`
...
12 |         n -= 1;
   |         ^^^^^^ cannot assign to immutable argument
error[E0384]: cannot assign to immutable argument `n`
```

#pause
#skip.large

```rust
fn fib(mut n: u32) -> Vec<u32> {
  //   ^^^
  ...
}
```

Jämför med C++: `vector<int> fib(const int n);` \
]

#slide(
  title: [Rekursiv fibonacci],
)[
$F_n = F_(n-1) + F_(n-2)$

#pause

```rust
fn fib(n: u32) -> u32 {
  if n == 0 {
    0
  } else if n == 1 {
    1
  } else {
    fib(n-1) + fib(n-2)
  }
}

fn main() {
  println!("{}", fib(12));
}
```
][
Den här koden är allmänt känd som megalångsam. Varje extra $n$ \~dubblar antalet
uträkningar.

Vi vill ha _memoization_.

```python
  cache = {}
  def fib(n):
    if n in cache:
      return cache[n]
    ret = fib(n-1) + fib(n-2)
    cache[n] = ret
    return ret
  ```

Vi testar!
]

#slide(title: [Memoization med globala variabler])[
```rust
use std::collections::HashMap;

let cache = HashMap::new();
fn fib(n: u32) -> u32 {
  if cache.contains_key(&n) {
    return cache[&n];
  }

  let ret = if n == 0 {
    0
  } else if n == 1 {
    1
  } else {
    fib(n-1) + fib(n-2)
  };
  cache.insert(n, ret);
  ret
}
```
][
#pause
#[
#set text(size: 0.8em)
```
error: expected item, found keyword `let`
 --> src/main.rs:3:1
  |
3 | let cache = HashMap::new();
  | ^^^ consider using `const` or `static`
        instead of `let` for global
        variables
```
]

#pause
`const` låter fel, så vi testar `static`.
]

#slide(title: [`let` $->$ `static`])[

`static cache = HashMap::new();`

#pause
#skip.large

```
 --> src/main.rs:3:16
  |
3 | static cache = HashMap::new();
  |                ^^^^^^^^^^^^ cannot infer type of the type
                   parameter `K` declared on the struct `HashMap`
  |
help: consider specifying the generic arguments
  |
3 | static cache = HashMap::<K, V>::new();
  |                       ++++++++

error: missing type for `static` item
 --> src/main.rs:3:13
  |
3 | static cache = HashMap::new();
  |             ^ help: provide a type for the item: `: <type>`

```

Två fel, men vi fixar båda samtidigt. ]

#slide(
  title: [`let` $->$ `static`],
)[
`static cache: HashMap<u32, u32> = HashMap::new();`

#pause

#text(
  size: 0.8em,
)[
```
error[E0015]: cannot call non-const fn `HashMap::<u32, u32>::new` in statics
 --> src/main.rs:3:35
  |
3 | static cache: HashMap<u32, u32> = HashMap::new();
  |                                   ^^^^^^^^^^^^^^
  |
  = note: calls in statics are limited to constant functions,
    tuple structs and tuple variants
  = note: consider wrapping this expression in `Lazy::new(|| ...)` from
    the `once_cell` crate: https://crates.io/crates/once_cell

error[E0596]: cannot borrow immutable static item `cache` as mutable
  --> src/main.rs:16:5
   |
16 |     cache.insert(n, ret);
   |     ^^^^^ cannot borrow as mutable

```
]

#grid(
  columns: (1fr, auto),
  [#pause Vi kan inte skapa vår HashMap, och vi kan inte modifiera den även om vi
    kunde skapa den. #pause],
  figure(
    caption: ["RUH ROH"],
    numbering: none,
    image(width: 3cm, "../../scooby.png"),
  ),
)
]

#slide(
  title: [Slutsatser],
)[
#line-by-line[
- Rust gillar inte statics och annat globalt state
- Det finns andra lösningar, men det beror på problemet du försöker lösa
- Bara för att kompilatorn ger en `hint:` betyder det inte att du kommer komma
  fram till rätt lösning. Fundera på vad du vill göra.
  #line-by-line(
    start: 4,
  )[
  - Vår rekursiva fibonacci behöver en `struct` som äger cache:en och att `fib` är
    en metod på instanser.
  - $==>$ Inte längre en global cache!
  ]
]
]
