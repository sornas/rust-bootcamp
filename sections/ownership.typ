#import "../theme.typ": *
#show: theme

#section[Ownership och move-semantics]

#slide(title: [Vad är ownership?])[
Varje värde har exakt en _owner_ vid varje programpunkt

```rust
fn count_numbers(s: String) -> u32 {
  let mut n = 0;
  for c in s.chars() {
    if c.is_numeric() {
      n += 1;
    }
  }
  n
}

fn count_letters(s: String) -> u32 { /* motsvarande */ }

fn main() {
  let content = std::fs::read_to_string("file.txt").unwrap();
  println!("number of numbers: {}", count_numbers(content));
  println!("number of letters: {}", count_letters(content));
}

```
]

#slide[
#set text(size: 0.8em)
```
error[E0382]: use of moved value: `content`
  --> count.rs:23:53
   |
21 |     let content = std::fs::read_to_string("file.txt").unwrap();
   |         ------- move occurs because `content` has type `String`, which
                     does not implement the `Copy` trait
22 |     println!("number of numbers: {}", count_numbers(content));
   |                                                     ------- value moved here
23 |     println!("number of letters: {}", count_letters(content));
   |                                                     ^^^^^^^ value used here
                                                                 after move
   |
note: consider changing this parameter type in function `count_numbers` to borrow instead if owning the value isn't necessary
  --> count.rs:1:21
   |
1  | fn count_numbers(s: String) -> u32 {
   |    -------------    ^^^^^^ this parameter takes ownership of the value
   |    |
   |    in this function
help: consider cloning the value if the performance cost is acceptable
   |
22 |     println!("number of numbers: {}", count_numbers(content.clone()));
   |                                                            ++++++++
```
]

#slide[
Vi får två val:

1. `consider changing this parameter type in function 'count_numbers' to borrow instead if owning the value isn't necessary`
2. `consider cloning the value if the performance cost is acceptable`
]

#slide[
`consider changing this parameter type in function 'count_numbers' to borrow instead if owning the value isn't necessary`

```rust
  fn count_numbers(s: &String) -> u32 {
    let mut n = 0;
    for c in s.chars() {
      if c.is_numeric() {
        n += 1;
      }
    }
    n
  }

  println!("{}", count_numbers(&content));
  ```
]

#slide[
`consider cloning the value if the performance cost is acceptable`

```rust
fn main() {
  let content = std::fs::read_to_string("file.txt").unwrap();
  println!("number of numbers: {}", count_numbers(content.clone()));
  println!("number of letters: {}", count_letters(content.clone()));
}
```
]

#slide(
  title: [Borrow eller clone?],
)[
Om du kan så är borrow uppenbarligen att föredra. Du kommer inte råka mutera en
`&T` på samma sätt som du kan råka mutera en `T`. Med en `&T` är det uppenbart
att du lånar ett `T`.

`clone()` är å andra sidan väldigt effektivt.
]

#slide(
  title: [Copy],
)[
Vissa typer är `Copy`, vilket är "Types whose values can be duplicated simply by
copying bits." \ (`https://doc.rust-lang.org/stable/std/marker/trait.Copy.html`)

```rust
fn is_even(n: u32) -> bool {
  n % 2 == 0
}

fn is_odd(n: u32) -> bool {
  n % 2 == 1
}

fn main() {
  let a = 11;
  println!("{}", is_even(a));
  println!("{}", is_odd(a));
}
```

En typ som är `Copy` kommer kopieras automatiskt
]

#slide(
  title: [Varför ownership?]
)[
  Microsofts "Security Response Centre" klassifierar alla rapporterade security vulnerabilities

  #image("../memory-safety.png")

  #text(size: 0.8em)[`https://msrc.microsoft.com/blog/2019/07/a-proactive-approach-to-more-secure-code/`\ 2019-07]

  Under perioden 2006--2018 var 70% av sårbarheter relaterade till "memory safety"
]

#slide(
  title: [I nyheterna]
)[
  #v(1fr)
  #align(center, image(width: 80%, "../so-in-rust-we-trust.png"))
  #v(2fr)
]

#slide(
  title: [Vad är memory safety?]
)[
  Samlingsnamn för olika typer av programmeringsfel

  - Buffer overflow och over-read
  - Race condition
  - Use-after-free
  - Double-free
  - Null pointer dereference
  - Uninitialized values
  - Stack overflow
  - Heap exhaustion

  (`https://en.wikipedia.org/wiki/Memory_safety`)
]

#slide(
  title: []
)[
Vad gör Rust?

- Minskar risken för vissa minnesrelaterade buggar
- Minskar mängden kod som kan introducera minnesosäkerhet
]
