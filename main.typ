#import "theme.typ": *
#show: theme

#title-slide(title: [Lektion 1+])

#base-slide(
  {
    utils.polylux-outline(enum-args: (numbering: (_) => [+]), padding: 0pt)
  },
)

// #include "sections/hello/cargo-new.typ"
// #include "sections/hello/1-1-2.typ"
// #include "sections/hello/3-5-8.typ"
// #include "sections/hello/typeinference.typ"
// #include "sections/hello/functions.typ"
//
// #include "sections/borrow.typ"
// #include "sections/ownership.typ"
// #include "sections/vec-option.typ"
// #include "sections/items.typ"
// #include "sections/trait1.typ"
// #include "sections/generics.typ"
// #include "sections/trait2.typ"
// #include "sections/iterator.typ"
// #include "sections/modules.typ"

#include "sections/toolchain.typ"
