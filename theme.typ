#import "@preview/polylux:0.3.1": *
#import "@preview/lovelace:0.2.0": *

#let darktheme = true

#let ferris = (
  among-us: box(image(height: 0.8em, "ferris-emoji/ferris-among-us.png")),
  exorcism: box(image(height: 0.8em, "ferris-emoji/ferris-exorcism.png")),
  gentleman: box(image(height: 0.8em, "ferris-emoji/ferris-gentleman.png")),
  grimace: box(image(height: 0.8em, "ferris-emoji/ferris-grimace.png")),
  hammer: box(image(height: 0.8em, "ferris-emoji/ferris-hammer.png")),
  heart-eyes: box(image(height: 0.8em, "ferris-emoji/ferris-heart-eyes.png")),
  joy: box(image(height: 0.8em, "ferris-emoji/ferris-joy.png")),
  pacman: box(image(height: 0.8em, "ferris-emoji/ferris-pacman.png")),
  party: box(image(height: 0.8em, "ferris-emoji/ferris-party.png")),
  pensive: box(image(height: 0.8em, "ferris-emoji/ferris-pensive.png")),
  plead: box(image(height: 0.8em, "ferris-emoji/ferris-plead.png")),
  police: box(image(height: 0.8em, "ferris-emoji/ferris-police.png")),
  think: box(image(height: 0.8em, "ferris-emoji/ferris-think.png")),
  thonk: box(image(height: 0.8em, "ferris-emoji/ferris-thonk.png")),
  unsafe: box(image(height: 0.8em, "ferris-emoji/ferris-unsafe.png")),
  what: box(image(height: 0.8em, "ferris-emoji/ferris-what.png")),
  wink: box(image(height: 0.8em, "ferris-emoji/ferris-wink.png")),
  wtf: box(baseline: 0.1em, image(height: 0.8em, "ferris-emoji/ferris-wtf.png")),
)

#let series-title = [Rust för plebs]
#let series-subtitle = [#ferris.grimace #sym.arrow.long #ferris.heart-eyes]
#let entry-author = [Gustav Sörnäs]

#let colormaps = (
  light: (bg: white.darken(10%), fg: black),
  dark: (bg: black.lighten(10%), fg: white),
)
#let color = if darktheme { colormaps.dark } else { colormaps.light }

#let skip = (small-back: v(-0.5em), tiny: v(0.2em), small: v(0.5em), large: v(1em))

#let theme(body) = {
  set page(paper: "presentation-4-3", fill: color.bg, header: none, footer: none)
  set text(font: "Atkinson Hyperlegible", size: 20pt, fill: color.fg, lang: "sv")
  set raw(
    theme: if darktheme { "ayu-rstudio/tmTheme/ayu-mirage.tmTheme" } else { "InspiredGitHub.tmtheme/InspiredGitHub.tmTheme" },
  )
  show: setup-lovelace

  body
}

#let base-slide(header: none, ..bodies) = {
  let bodies = bodies.pos()
  let columns = { (1fr,) * bodies.len() }
  let body = pad(0pt, grid(columns: columns, ..bodies))

  let footer = {}

  set page(
    margin: (x: 2cm, top: 3cm, bottom: 1cm),
    header: header,
    footer: footer,
    footer-descent: 0cm,
    header-ascent: 1cm,
  )

  polylux-slide(body)
}

#let title-slide(title: []) = {
  base-slide({
    set align(horizon)
    block({ text(size: 1.8em, strong(series-title)) })
    skip.small-back
    block({ text(size: 1em, series-subtitle) })
    v(1.5em)
    block({ text(size: 1.2em, title) })
    skip.small-back
    block({ text(size: 0.8em, entry-author) })
    v(1fr)
  })
}

#let section(..content) = {
  let content = content.pos()
  utils.register-section(content.at(0))

  base-slide({
    set align(horizon)
    block({ text(size: 1.2em, content.at(0)) })
    if content.len() > 1 {
      block({ text(content.at(1)) })
    }
    v(1fr)
  })
}

#let slide(title: none, ..bodies) = {
  let header = {
    grid(columns: (1fr, auto), title, utils.current-section)
    v(-0.8em)
    line(length: 100%, stroke: color.fg + 0.5pt)
  }
  base-slide(header: header, ..bodies)
}

#let slide-centered(title: none, body) = {
  slide(title: title, align(horizon, {
    body
    v(1fr)
  }))
}

#show: theme
