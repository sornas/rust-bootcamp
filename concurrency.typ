#import "theme.typ": *
#show: theme

#title-slide(title: [Concurrency i Rust])

#base-slide(
  {
    utils.polylux-outline(enum-args: (numbering: (_) => [+]), padding: 0pt)
  }
)

#include "sections/concurrency/intro.typ"
#include "sections/concurrency/thread.typ"
#include "sections/concurrency/async.typ"
#include "sections/concurrency/sharing.typ"
#include "sections/concurrency/thread-plus-async.typ"
#include "sections/toolchain.typ"
